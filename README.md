# Coumana Database model & migrations

This is the Coumana Database project

## Add git flow:
```
git flow init
Branch name for production releases: [master] 
Branch name for "next release" development: [develop] 

How to name your supporting branch prefixes?
Feature branches? [] feature/
Bugfix branches? [] bugfix/
Release branches? [] release/
Hotfix branches? [] hotfix/
Support branches? [] support/
Version tag prefix? [] 
```

## Run mysql and DB to test backend/frontend
```bash
docker-compose up mysql
```
